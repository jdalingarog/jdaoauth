//
//  JDAOauthSignature.m
//  Xplorama
//
//  Created by John Dwaine Alingarog on 4/23/14.
//  Copyright (c) 2014 ERNI Development Center Philippines Inc. All rights reserved.
//

#import "JDAOauthSignature.h"
#import "hmac.h"
#include "Base64Transcoder.h"

@implementation JDAOauthSignature

#pragma mark - Public methods

+ (NSString *)signatureWithHTTPRequestMethod:(NSString *)requestMethod
                                     baseURL:(NSString *)baseURL
                                  parameters:(NSDictionary *)parameters
                                 secretToken:(NSString *)secretToken
                              secretConsumer:(NSString *)secretConsumer
{
    NSString *signature = nil;
    NSArray *parameterKeys = [parameters.allKeys sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    NSMutableString *signatureMutableBaseString = [NSMutableString string];
    
    
    //Create signature base string
    [signatureMutableBaseString appendString:requestMethod.uppercaseString];
    [signatureMutableBaseString appendString:@"&"];
    [signatureMutableBaseString appendString:[self URLEncodedString_chWithStirng:baseURL]];
    [signatureMutableBaseString appendString:@"&"];
    
    //collect parameters
    NSMutableString *parameterMutableString = [NSMutableString string];
    for (NSString *key in parameterKeys) {
        NSString *parameterKeyValue = [NSString stringWithFormat:@"%@=%@", [self URLEncodedString_chWithStirng:key], [self URLEncodedString_chWithStirng:parameters[key]]];
        [parameterMutableString appendString:parameterKeyValue];
        if ([parameterKeys indexOfObject:key] < parameterKeys.count - 1) [parameterMutableString appendString:@"&"];
    }
    
    [signatureMutableBaseString appendString:[self URLEncodedString_chWithStirng:parameterMutableString.copy]];
    
    //calculate signature
    NSString *requestToken = [NSString stringWithFormat:@"%@&%@", [self URLEncodedString_chWithStirng:secretConsumer], [self URLEncodedString_chWithStirng:secretToken]];
    signature = [self signClearText:signatureMutableBaseString.copy withSecret:requestToken];
    
    return signature;
}

+ (NSString *)timestampString
{
    return [[NSString alloc]initWithFormat:@"%ld", time(NULL)];
}

+ (NSString *)nonceString
{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    NSString *theString = (__bridge NSString *)string;
    CFRelease(string);
    return theString;
}

#pragma mark - Private methods

//Hash clear text with secret to hmac_sha1
+ (NSString *)signClearText:(NSString *)text withSecret:(NSString *)secret {
    NSData *secretData = [secret dataUsingEncoding:NSUTF8StringEncoding];
    NSData *clearTextData = [text dataUsingEncoding:NSUTF8StringEncoding];
    unsigned char result[20];
    hmac_sha1((unsigned char *)[clearTextData bytes], [clearTextData length], (unsigned char *)[secretData bytes], [secretData length], result);
    
    //Base64 Encoding
    char base64Result[32];
    size_t theResultLength = 32;
    Base64EncodeData(result, 20, base64Result, &theResultLength);
    NSData *theData = [NSData dataWithBytes:base64Result length:theResultLength];
    
    NSString *base64EncodedResult = [[NSString alloc] initWithData:theData encoding:NSUTF8StringEncoding];
    
    return base64EncodedResult;
}

//Percent encode string
+ (NSString *)URLEncodedString_chWithStirng:(NSString *)string {
    NSMutableString * output = [NSMutableString string];
    const unsigned char * source = (const unsigned char *)[string UTF8String];
    NSInteger sourceLen = strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

@end
