//
//  JDAOauthSignature.h
//  Xplorama
//
//  Created by John Dwaine Alingarog on 4/23/14.
//  Copyright (c) 2014 ERNI Development Center Philippines Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JDAOauthSignature : NSObject

//Generates oauth_signature
+ (NSString *)signatureWithHTTPRequestMethod:(NSString *)requestMethod
                                     baseURL:(NSString *)baseURL
                                  parameters:(NSDictionary *)parameters
                                 secretToken:(NSString *)secretToken
                              secretConsumer:(NSString *)secretConsumer;

//Returns current timestamp string
+ (NSString *)timestampString;

//Returns random unique string
+ (NSString *)nonceString;

@end
